#!/usr/bin/env bash

previous=0
increases=-1

lines=$(cat input)
for current in $lines
do
  if (( $current > $previous )); then
    increases=$(($increases+1))
  fi
  previous=$current
done
echo $increases
