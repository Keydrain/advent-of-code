#!/usr/bin/env bash

sequencer=0
values=()

A=0
B=0
C=0
D=0

lines=$(cat input)
for current in $lines
do

  A=$(($A+current))
  B=$(($B+current))
  C=$(($C+current))
  D=$(($D+current))

  # echo "$A and $B and $C and $D and $sequencer"

  case $sequencer in
    0)
      B=0
      values+=($C)
      sequencer=1
      ;;
    1)
      C=0
      values+=($D)
      sequencer=2
      ;;
    2)
      D=0
      values+=($A)
      sequencer=3
      ;;
    3)
      A=0
      values+=($B)
      sequencer=0
      ;;
  esac
done

values=("${values[@]:2}")

previous=0
increases=-1

for value in "${values[@]}"
do
  if (( $value > $previous )); then
    increases=$(($increases+1))
  fi
  previous=$value
done
echo $increases

