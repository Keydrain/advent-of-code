#!/usr/bin/env bash

gamma=""
epsilon=""

for (( i=0; i<12; i++ )); do
  zeros=0
  ones=0
  while IFS= read -r line; do
    value=$(echo ${line:$i:1})
    case $value in
      0)
        zeros=$(($zeros+1))
        ;;
      1)
        ones=$(($ones+1))
        ;;
    esac
  done < input
  gamma="${gamma}$(($zeros > $ones ? 0 : 1))"
  epsilon="${epsilon}$(($zeros > $ones ? 1 : 0))"
done

echo "GAMMA: $gamma $((2#$gamma))"
echo "EPSILON: $epsilon $((2#$epsilon))"

echo $((2#$gamma*2#$epsilon))
