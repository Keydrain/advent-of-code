#!/usr/bin/env bash

horizontal=0
depth=0

while IFS= read -r line; do
  direction=$(echo $line | awk '{print $1}')
  speed=$(echo $line | awk '{print $2}')

  case $direction in
    forward)
      horizontal=$(($horizontal+$speed))
      ;;
    down)
      depth=$(($depth+$speed))
      ;;
    up)
      depth=$(($depth-$speed))
      ;;
  esac
done < input

echo "$horizontal x $depth = $(($horizontal*$depth))"
